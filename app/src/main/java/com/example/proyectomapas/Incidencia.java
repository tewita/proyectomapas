package com.example.proyectomapas;

public class Incidencia {
    String latitud;
    String longitud;
    String direccio;
    String tipoagresion;
    String problema;
    String url;
    String nacionalidad;

    public Incidencia(String latitud, String longitud, String direccio,String tipoagresion,
                      String problema, String url,String nacionalidad) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.direccio = direccio;
        this.tipoagresion=tipoagresion;
        this.problema = problema;
        this.url = url;
        this.nacionalidad=nacionalidad;
    }

    public Incidencia() {
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDireccio() {
        return direccio;
    }

    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    public String getTipoagresion() {
        return tipoagresion;
    }

    public void setTipoagresion(String tipoagresion) {
        this.tipoagresion = tipoagresion; }

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
}